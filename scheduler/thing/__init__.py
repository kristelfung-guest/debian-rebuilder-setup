#!/usr/bin/python3
'''
<Module Name>
  srebuild-monitor.py

<Author>
  Kristel Fung <kristelfung@nyu.edu>

<Started>
  July 30, 2019

<Copyright>
  See LICENSE for licensing information.

<Purpose>
  Monitors buildinfos.debian.net for new buildinfos and queues them.
'''

import bs4
import requests
import redis
import hashlib
import datetime
import time
import sys
import pickle
import os

BASE_URL = 'https://buildinfos.debian.net/ftp-master.debian.org/buildinfo'

class Node:
  '''
  <Purpose>
    Node class used to store buildinfos in a tree structure.

  '''
  def __init__(self, name, children):
    self.name = name
    self.children = children
    self.buildinfo = None
    self.hash = None
    self.etag = None


def is_leaf(url):
  '''
  <Purpose>
    Checks if a page is a buildinfo or not (a leaf in the tree).

  <Arguments>
    url:
            The URL of a directory/buildinfo from buildinfos.debian.net.

  <Exceptions>
    None.

  <Side Effects>
    None.

  <Returns>
    True if page is a buildinfo, False if it is a directory.

  '''
  header = requests.get(url).headers
  if 'Content-Type' in header:  # buildinfos have no Content-Type
    return False
  return True


def get_folders(url):
  '''
  <Purpose>
    Finds directories within the current directory.

  <Arguments>
    url:
            The URL of a directory from buildinfos.debian.net.

  <Exceptions>
    None.

  <Side Effects>
    None.

  <Returns>
    List of subdirectories within the directory.

  '''
  page_contents = requests.get(url).text
  soup = bs4.BeautifulSoup(page_contents, 'lxml')
  rows = soup.findAll('tr')
  folders = []
  icons = ['/icons/unknown.gif', '/icons/folder.gif']

  for tr in rows:  # add folder names to folders list
    if tr.find('img'):
      if tr.find('img')['src'] in icons:
        folders.append(tr.find('a').contents[0].rstrip('/'))
  return folders


def get_name(url):
  '''
  <Purpose>
    Finds the name of the current directory (folder).

  <Arguments>
    url:
            The URL of a directory from buildinfos.debian.net.

  <Exceptions>
    None.

  <Side Effects>
    None.

  <Returns>
    Name of the directory as a string.

  '''
  return url.rsplit('/', 1)[1]


def create_tree(url):
  '''
  <Purpose>
    Creates a tree of Nodes corresponding to the folder structure of
    buildinfos.

  <Arguments>
    url:
            The URL of a directory from buildinfos.debian.net.

  <Exceptions>
    None.

  <Side Effects>
    None.

  <Returns>
    Node of buildinfos.

  '''
  if is_leaf(url):
    n = Node(get_name(url), [])  # create the Node
    response = requests.get(url)  # get buildinfo
    n.buildinfo = response.text
    return n
  else:
    children = []
    for folder in get_folders(url)[:4]:
      print(url + '/' + folder)
      children.append(create_tree(url + '/' + folder))
    n = Node(get_name(url), children)
    #response = requests.head(url)
    #n.etag = response.headers['etag']
    return n


def hash_tree(root):
  '''
  <Purpose>
    Fills tree of Nodes with hashes based off the hashes of buildinfos.

  <Arguments>
    root:
            A tree of Nodes.

  <Exceptions>
    None.

  <Side Effects>
    None.

  <Returns>
    None.

  '''
  if len(root.children) == 0:
    root.hash = hashlib.sha256(root.buildinfo.encode('ascii')).hexdigest()
    return root.hash
  else:
    h = hashlib.sha256()
    for c in root.children:
      h.update(hash_tree(c).encode('ascii'))
    root.hash = h.hexdigest()
    return root.hash


def add_leaves(root):
  '''
  <Purpose>
    Adds all buildinfos in a tree to the list 'res'.

  <Arguments>
    root:
            Tree of buildinfos.

  <Exceptions>
    None.

  <Side Effects>
    None.

  <Returns>
    None.

  '''
  res = []
  if root.buildinfo:
      res.append(root.buildinfo)
  else:
    for child in root.children:
      res.extend(add_leaves(child))
  return res


def compare_trees(old, new):
  '''
  <Purpose>
    Compares an old and new tree of buildinfos and returns a list of
    new buildinfos.

  <Arguments>
    old:
            The previous tree of buildinfos.
    new:
            The new tree of buildinfos.

  <Exceptions>
    None.

  <Side Effects>
    None.

  <Returns>
    List of new buildinfos that were not previously found in the
    old tree.

  '''
  res = []


  def _compare_trees(old, new):
    '''
    <Purpose>
      Helper function that recurses through an old and new buildinfo tree
      and adds new buildinfos to the list 'res'.

    <Arguments>
      old:
              Tree of old buildinfos.
      new:
              Tree of new buildinfos.

    <Exceptions>
      None.

    <Side Effects>
      None.

    <Returns>
      None.

    '''
    if len(old.children) == 0 and len(new.children) == 0:  # reached a leaf (buildinfo)
      if old.hash != new.hash:
        res.append(new.buildinfo)
      return
    if len(old.children) < len(new.children):  # if new has extra children
      for i in range(len(old.children), len(new.children)):
        res.extend(add_leaves(new.children[i]))
    for i in range(len(old.children)):  # check corresponding nodes
      if old.children[i].hash != new.children[i].hash:
        return _compare_trees(old.children[i], new.children[i])

  _compare_trees(old, new)

  return res


def main():
  # the queue
  db = redis.StrictRedis(host='localhost', port=6379, db=0)

  now = datetime.datetime.now()
  year = str(now.year)

  # check if old builds exist
  if not os.path.exists('old_builds.pickle'):
    old_builds = Node('empty_tree', [])
    with open('old_builds.pickle', 'wb') as f:
      pickle.dump(old_builds, f, pickle.HIGHEST_PROTOCOL)

  # loop - run every 2 hours
  while True:
    print('[*] Constructing new buildinfo tree')

    now = datetime.datetime.now()
    year = str(now.year)
    new_builds = create_tree(BASE_URL + '/' + year + '/10')
    hash_tree(new_builds)

    print('[*] Comparing new and old tree to find new buildinfos')
    if new_builds:
      # create old_builds from old_builds.pickle
      with open('old_builds.pickle', 'rb') as f:
        old_builds = pickle.load(f)
      new_buildinfos = compare_trees(old_builds, new_builds)

    print('[+] Adding new buildinfos to the queue')
    for build in new_buildinfos:
      print('buildinfo')
      db.rpush('rebuild-q', build)

    # once all builds downloaded we update the old tree
    # old_builds = new_builds

    # save old_builds in a file
    with open('old_builds.pickle', 'wb') as f:
      pickle.dump(new_builds, f, pickle.HIGHEST_PROTOCOL)

    print('[*] Sleeping zZz')
    time.sleep(14400)


if __name__ == '__main__':
  main()
